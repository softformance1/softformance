$(document).ready(function(){
	/* Form Button Handlers */
	$('.send-ajax').click( function() {
		let button = $(this);
		button.prop('disabled', true);
		button.addClass('disabled');
		
		let form = button.closest('form');
		let redirect = form.find('input[name="redirect"]').val();
		let thanks = form.find('input[name="thanks_modal"]').val();
		
		if ( form[0].checkValidity() ) {
			form.css('opacity','.5');
			let actUrl = form.attr('action');

			$.ajax({
				url: actUrl,
				type: 'post',
				dataType: 'html',
				data: form.serialize(),
				success: function(data) {
					form.removeClass('was-validated');
					button.prop('disabled', false);
					button.removeClass('disabled');
					form.css('opacity','1');
					
					if ( redirect ) {
						window.location.href = redirect;
					}
					
					if ( thanks ) {
						$('.modal').modal('hide');
						
						setTimeout(function(){
							$(thanks).modal('show');
							
							setTimeout(function(){
								$(thanks).modal('hide');
							},5000);
						},150);
					}
				},
				error: function() {
				}
			});
		} else {
			form.addClass('was-validated');
			button.prop('disabled', false);
			button.removeClass('disabled');
		}
	});
	
	$('.send').click( function() {
		let button = $(this);
		let form = button.closest('form');
		
		button.prop('disabled', true);
		button.addClass('disabled');

		if ( form[0].checkValidity() ) {
			form.removeClass('was-validated');
			form.submit();
		} else {
			form.addClass('was-validated');
			button.prop('disabled', false);
			button.removeClass('disabled');
		}
	});
	
	$(".go-to-block").click(function() {
		var target = $(this).data('target');
		
	    $('html, body').animate({
	        scrollTop: $(target).offset().top - 30
	    }, 400);
	});
});