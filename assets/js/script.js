let queryString = window.location.search.slice(1);

$(document).ready(function () {

	$(".color-dots-wrapper").paroller({
		factorXs: 0.1, // multiplier for scrolling speed and offset
		factor: 0.2, // multiplier for scrolling speed and offset
		type: 'foreground', // background, foreground
		direction: 'vertical', // vertical, horizontal
		transition: 'transform 1.3s cubic-bezier(0.08, 1.15, 1, 1) 0s' // CSS transition
	});

	/* Form Button Handlers */
	$('.send-ajax').click(function () {
		let button = $(this);
		button.prop('disabled', true);
		button.addClass('disabled');

		let form = button.closest('form');
		let redirect = form.find('input[name="redirect"]').val();
		let thanks = form.find('input[name="thanks_modal"]').val();

		if (form[0].checkValidity()) {
			form.css('opacity', '.5');
			let actUrl = form.attr('action');

			$.ajax({
				url: actUrl,
				type: 'post',
				dataType: 'html',
				data: form.serialize(),
				success: function (data) {
					form.removeClass('was-validated');
					button.prop('disabled', false);
					button.removeClass('disabled');
					form.css('opacity', '1');

					if (redirect) {
						window.location.href = redirect;
					}

					if (thanks) {
						$('.modal').modal('hide');

						setTimeout(function () {
							$(thanks).modal('show');

							setTimeout(function () {
								$(thanks).modal('hide');
							}, 5000);
						}, 150);
					}
				},
				error: function () {}
			});
		} else {
			form.addClass('was-validated');
			button.prop('disabled', false);
			button.removeClass('disabled');
		}
	});

	$('.send').click(function () {
		let button = $(this);
		let form = button.closest('form');

		button.prop('disabled', true);
		button.addClass('disabled');

		if (form[0].checkValidity()) {
			form.removeClass('was-validated');
			form.submit();
		} else {
			form.addClass('was-validated');
			button.prop('disabled', false);
			button.removeClass('disabled');
		}
	});

	$(".go-to-block").click(function (e) {
		e.preventDefault();
		let target = $(this).data('target');

		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 400);
	});

	/* Set UTM */
	let urlParams = getUrlParams();

	if (urlParams.utm_campaign) {
		$('form').find('input[name="utm_campaign"]').val(urlParams.utm_campaign);
	}
	if (urlParams.utm_content) {
		$('form').find('input[name="utm_content"]').val(urlParams.utm_content);
	}
	if (urlParams.utm_medium) {
		$('form').find('input[name="utm_medium"]').val(urlParams.utm_medium);
	}
	if (urlParams.utm_source) {
		$('form').find('input[name="utm_source"]').val(urlParams.utm_source);
	}
	if (urlParams.utm_term) {
		$('form').find('input[name="utm_term"]').val(urlParams.utm_term);
	}
	if (urlParams.utm_group) {
		$('form').find('input[name="utm_group"]').val(urlParams.utm_group);
	}
	$('form').find('input[name="http_referer"]').val(window.location.href);
});

function getUrlParams(c) {
	var b = c ? c.split("?")[1] : window.location.search.slice(1);
	var e = {};
	if (b) {
		b = b.split("#")[0];
		var h = b.split("&");
		for (var f = 0; f < h.length; f++) {
			var k = h[f].split("=");
			var g = k[0];
			var d = typeof (k[1]) === "undefined" ? true : k[1];
			g = g.toLowerCase();
			if (typeof d === "string") {
				d = d.toLowerCase()
			}
			if (g.match(/\[(\d+)?\]$/)) {
				var l = g.replace(/\[(\d+)?\]/, "");
				if (!e[l]) {
					e[l] = []
				}
				if (g.match(/\[\d+\]$/)) {
					var j = /\[(\d+)\]/.exec(g)[1];
					e[l][j] = d
				} else {
					e[l].push(d)
				}
			} else {
				if (!e[g]) {
					e[g] = d
				} else {
					if (e[g] && typeof e[g] === "string") {
						e[g] = [e[g]];
						e[g].push(d)
					} else {
						e[g].push(d)
					}
				}
			}
		}
	}
	return e
};

$(window).on('load', function () {
	if ($(window).width() > 767) {
		setHeight('.modules', '.module', '.slider');
	}

	let modulesPagination = [];

	$('.swiper-pagination div').each(function () {
		modulesPagination.push($(this).text());
	});

	let swiperModules = new Swiper('.swiper-modules', {
		slidesPerView: 1,
		spaceBetween: 10,
		mousewheel: true,

		breakpoints: {
			0: {
				autoHeight: true,
			},
			768: {
				direction: 'vertical',
				autoHeight: false,
			},
		},

		pagination: {
			el: '.modules-pagination',
			type: 'bullets',
			clickable: true,
			renderBullet: function (index, className) {
				return '<span class="' + className + '">' + (modulesPagination[index]) + '</span>';
			},
		},
		navigation: {
			nextEl: '.modules-next',
			prevEl: '.modules-prev',
		},
	});

});

function setHeight(parent, block, slider = 0) {
	$(parent).each(function () {
		var height = 0,
			blockk = $(this).find(block);


		blockk.each(function () {
			var blockHeight = $(this).outerHeight();
			if (height < blockHeight) {
				height = blockHeight;
			}
		});
		blockk.css({
			height: height
		});

		if ($(slider) != 0) {
			let sliderBlock = $(this).find(slider);

			$(sliderBlock).css({
				height: height
			});
		}
	});
}

// Loads the IFrame Player API code asynchronously.
let tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

let player;

function onYouTubeIframeAPIReady() {
	let videoID = document.querySelector('.min-video.active').getAttribute('data-id');//get active video

	player = new YT.Player('player', {
		playerVars: {
			'controls': 0,
			'showinfo': 0,
			'rel': 0,
			'autoplay': 1
		},
		videoId: videoID,
		events: {
			//'onStateChange': onPlayerStateChange,
			//'onReady': onPlayerReady,
		}
	});
}

$('.min-video').click(function () {
	let id = $(this).data('id'),
		bg = $(this).attr('style');

	player.loadVideoById(id);

	$('.min-video').removeClass('pe-none active');
	$(this).addClass('pe-none active');
	$('.to-play').attr('style', bg);
	$('.to-play').addClass('active');
});

$(document).on('click', '.to-play', function () {

	var state = player.getPlayerState(),
		toggle = $(this);

	if (state !== 1) {
		player.playVideo();
		toggle.addClass('active');

	} else {
		player.pauseVideo();
		toggle.removeClass('active');
	}

});