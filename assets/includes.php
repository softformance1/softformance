<?php
/* MAIN CSS */
$f3->set(
	'UI_CSS_FILES',
	array(
		'assets/plugins/bootstrap/css/bootstrap.min.css',
		'assets/plugins/bootstrap/icons/bootstrap-icons.css',
		'assets/plugins/swiper/swiper.min.css',
		'assets/css/template.css'
	)
);

/* MAIN JS */
$f3->set(
	'UI_JS_FILES',
	array(
		'assets/plugins/jquery/jquery-3.5.1.min.js',
		'assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
		'assets/plugins/counter/jquery.plugin.js',
		'assets/plugins/swiper/swiper.min.js',
		'assets/plugins/paroller/jquery.paroller.min.js',
		'assets/js/script.js'
	)
);

/* ADMIN CSS */
$f3->set(
	'UI_ADMIN_CSS_FILES',
	array(
		'assets/plugins/bootstrap/css/bootstrap.min.css',
		'assets/plugins/bootstrap/icons/bootstrap-icons.css',
		'assets/css/admin.css'
	)
);

/* ADMIN JS */
$f3->set(
	'UI_ADMIN_JS_FILES',
	array(
		'assets/plugins/jquery/jquery-3.5.1.min.js',
		'assets/plugins/bootstrap/js/bootstrap.bundle.min.js',
		'assets/js/admin.js'
	)
);