<?php
class ScssList {
	public static $files = [
		[
			'css' => 'template.css',
			'scss' => [
				'utilites.scss',
				'mixin.scss',
				'fonts.scss',
				'template.scss',
				'style.scss'
			]
		],
		/*[
			'css' => 'example.css',
			'scss' => [
				'utilites.scss',
				'mixin.scss',
				'template.scss',
				'example.scss'
			]
		],*/
	];
	
	public static function Instance() {
		return self::$files;
	}
}
return ScssList::Instance();