<!DOCTYPE html>
<html>

<head>
    <?php include 'blocks/head.php'; ?>
</head>

<body>
    <div class="wrapper">
        <header class="header position-relative color-white overflow-hidden">
            <div class="obj obj-1 position-absolute bottom-0 start-50 translate-middle-x">
                <img src="<?php echo $baseurl; ?>/assets/images/shadow-line.png" alt="shadow">
            </div>
            <div class="container">
                <div class="logo text-center">
                    <img src="<?php echo $baseurl; ?>/assets/images/logo.svg" alt="logo">
                </div>
                <div class="h-row">
                    <div class="row align-items-end">
                        <div class="col-12 col-lg-6">
                            <div class="h-inner">
                                <div class="for-title">
                                    <p class="info">4 Days online course</p>
                                    <h1 class="title fs-xxl-52 fs-xl-45 fs-lg-37 fs-md-40 fs-25 mt-3 lh-1">
                                        Take your first smart <br class="d-none d-xl-inline"> steps to implement your own software application in just 4 days
                                    </h1>
                                    <div class="text">
                                        <p class="mt-4">How to build your own software product: from idea to launch. <br class="d-none d-xl-inline"> For agency owners, consultants, coaches and business owners</p>
                                    </div>
                                </div>
                                <div class="for-price fw-bold">
                                    <div class="price fs-xl-27 fs-md-23 fs-20">
                                        Regular course fee: <nobr>399 $</nobr>
                                    </div>
                                    <p class="text-decoration-underline mt-3">Get it for free</p>
                                </div>
                                <div class="for-btn d-lg-flex align-items-center">
                                    <div class="order me-lg-3">
                                        <a class="cl-btn" data-bs-toggle="modal" data-bs-target="#reg-messangers" href="#">
                                            Via Messengers
                                        </a>
                                    </div>
                                    <div class="order mt-3 mt-lg-0">
                                        <a class="cl-btn" data-bs-toggle="modal" data-bs-target="#reg-email" href="#">
                                            Via e-mail
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 d-lg-flex justify-content-lg-center">
                            <div class="for-photo position-relative ms-xl-5">
                                <div class="circle position-absolute start-50 top-50 translate-middle">
                                    <img src="<?php echo $baseurl; ?>/assets/images/photo-circle.png" alt="circle">
                                </div>
                                <div class="photo">
                                    <img src="<?php echo $baseurl; ?>/assets/images/main-photo.png" alt="photo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>

            <section class="will position-relative">
                <div class="obj obj-1 position-absolute start-50 top-50 translate-middle h-100 d-none d-md-block">
                    <img class="h-100" src="<?php echo $baseurl; ?>/assets/images/circles.svg" alt="circles">
                </div>
                <div class="container">
                    <h2 class="title fs-xl-40 fs-md-30 fs-25 fw-bold color-blue text-center">
                        Over the course of 4 days you will learn how:
                    </h2>
                    <div class="w-row">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="w-block">
                                    <div class="pic">
                                        <img src="<?php echo $baseurl; ?>/assets/images/o1.svg" alt="pic">
                                    </div>
                                    <div class="text">
                                        <p>To avoid mistakes that 90% of software customers make</p>
                                        <p>To plan a project based on expertise</p>
                                        <p>To stay within the budget and timescales</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="w-block">
                                    <div class="pic">
                                        <img src="<?php echo $baseurl; ?>/assets/images/o1.svg" alt="pic">
                                    </div>
                                    <div class="text">
                                        <p>To generate software ideas on autopilot and be sure people need them</p>
                                        <p>To choose an audience and interact with it</p>
                                        <p>To pick only technically feasible ideas</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="w-block">
                                    <div class="pic">
                                        <img src="<?php echo $baseurl; ?>/assets/images/o1.svg" alt="pic">
                                    </div>
                                    <div class="text">
                                        <p>To think over a strategy from A to Z so that you do not waste your time and money</p>
                                        <p>To engage and hold your audience</p>
                                        <p>To make money with your software</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="w-block">
                                    <div class="pic">
                                        <img src="<?php echo $baseurl; ?>/assets/images/o1.svg" alt="pic">
                                    </div>
                                    <div class="text">
                                        <p>To scope out and estimate your first tech project version (MVP)</p>
                                        <p>To start the development on right foot</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order text-center">
                            <a class="cl-btn" href="#">
                                Get course for free
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="you color-white">
                <div class="container">
                    <h2 class="title fs-xl-35 fs-md-30 fs-23 fw-bold">
                        Mini-course for agency owners, consultants, coaches and <br class="d-none d-xl-inline"> service providers that want to launch own software product:
                    </h2>
                    <div class="tit fw-bold fs-xl-30 fs-md-25 fs-18 mt-4 mt-md-5">
                        You need this course if:
                    </div>
                    <div class="y-row">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="y-block d-flex align-items-center">
                                    <div class="icon flex-shrink-0 me-3 me-md-4" data-number="1">
                                        <img src="<?php echo $baseurl; ?>/assets/images/fig.svg" alt="fig">
                                    </div>
                                    <div class="text">
                                        <p>You are business owner (*agency owner) and you want to get a basic understanding of how to create and successfully launch your product and sell it</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="y-block d-flex align-items-center">
                                    <div class="icon flex-shrink-0 me-3 me-md-4" data-number="2">
                                        <img src="<?php echo $baseurl; ?>/assets/images/fig.svg" alt="fig">
                                    </div>
                                    <div class="text">
                                        <p>You are a consultant and want to build recurring revenue stream upselling software product to your existing clients</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="y-block d-flex align-items-center">
                                    <div class="icon flex-shrink-0 me-3 me-md-4" data-number="3">
                                        <img src="<?php echo $baseurl; ?>/assets/images/fig.svg" alt="fig">
                                    </div>
                                    <div class="text">
                                        <p>You are an expert helping clients to achieve their goals (Coach) and you want to automate your methods and make them as convenient for a large number of clients as ossible</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="y-block d-flex align-items-center">
                                    <div class="icon flex-shrink-0 me-3 me-md-4" data-number="4">
                                        <img src="<?php echo $baseurl; ?>/assets/images/fig.svg" alt="fig">
                                    </div>
                                    <div class="text">
                                        <p>Owner of a consulting or marketing agency and you want to streamline your agency processes and serve your clients better with web or mobile app</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="about pb-5 mb-md-5">
                <div class="container">
                    <div class="about-rows">
                        <div class="a-row">
                            <div class="row align-items-end">
                                <div class="col-12 col-md-5 text-center">
                                    <div class="photo">
                                        <img class="mw-100" src="<?php echo $baseurl; ?>/assets/images/ph1.jpg" alt="photo">
                                    </div>
                                </div>
                                <div class="col-12 col-md">
                                    <div class="info">
                                        <p class="post fs-xl-18 fs-16 mb-1">Author and speaker of the Course</p>
                                        <div class="name fs-xl-40 fs-md-35 fs-30 color-blue mb-3 mb-md-4 fw-bold">
                                            Vitaliy Podoba
                                        </div>
                                        <ul class="list">
                                            <li>CEO and founder of software development company SoftFormance</li>
                                            <li>Former Python/Django programmer</li>
                                            <li>More than 15 years of experience in IT</li>
                                            <li>Author of the book "Web Development with Python and Django for Beginners"</li>
                                            <li>Core Contributor to number of open source projects (e.g. Plone CMS)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="a-row">
                            <div class="row align-items-end flex-md-row-reverse">
                                <div class="col-12 col-md-5 text-center">
                                    <div class="photo">
                                        <img class="mw-100" src="<?php echo $baseurl; ?>/assets/images/ph2.jpg" alt="photo">
                                    </div>
                                </div>
                                <div class="col-12 col-md">
                                    <div class="info">
                                        <p class="post fs-xl-18 fs-16 mb-1">Speaker of the Course</p>
                                        <div class="name fs-xl-40 fs-md-35 fs-30 color-blue mb-3 mb-md-4 fw-bold">
                                            Ethan Sigmon
                                        </div>
                                        <ul class="list">
                                            <li>6 years in Real Estate Industry</li>
                                            <li>Founder of digital marketing agency “Advengers”</li>
                                            <li>Mentor of “How to successfully generate leads from <br class="d-none d-xl-inline"> social media and online ads”</li>
                                            <li>Co-founder of SocialInsight.io</li>
                                            <li>Co-founder of Opesta.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <footer class="footer">
            <div class="container">
                <h2 class="title fs-xl-40 fs-md-30 fs-24 mb-4">
                    Take the course and get a step-by-step plan how <br class="d-none d-xl-inline"> to build and launch software for your business
                </h2>
                <div class="f-row">
                    <div class="row align-items-end">
                        <div class="col-12 col-md-6">
                            <div class="f-left">
                                <div class="text fs-xl-25 fs-md-20 fs-18 mb-4">
                                    To take a place on the course, choose how you <br class="d-none d-xl-inline"> want to access video tutorials and materials:
                                </div>
                                <div class="price mb-2">
                                    Regular course fee: <strike>
                                        <nobr>399 $</nobr>
                                    </strike>
                                </div>
                                <div class="tit fs-xl-35 fs-md-30 fs-25 fw-bold">
                                    Get it for free
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 text-md-end mt-4 mt-md-0">
                            <div class="f-right">
                                <div class="for-btn d-lg-inline-flex align-items-center">
                                    <div class="order me-lg-3">
                                        <a class="cl-btn" data-bs-toggle="modal" data-bs-target="#reg-messangers" href="#">
                                            Via Messengers
                                        </a>
                                    </div>
                                    <div class="order mt-3 mt-lg-0">
                                        <a class="cl-btn" data-bs-toggle="modal" data-bs-target="#reg-email" href="#">
                                            Via e-mail
                                        </a>
                                    </div>
                                </div>
                                <div class="link mt-4 mt-md-5">
                                    <a target="_blank" href="#">Privacy policy </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <?php include 'blocks/modals.php'; ?>

    <?php include 'blocks/scripts.php'; ?>

    <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="ratio ratio-16x9">
                    <div id="player"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;



        $(document).ready(function() {
            $('#video-modal').on('shown.bs.modal', function(e) {
                let button = $(e.relatedTarget),
                    video = button.data('video');

                if (player) {
                    if (player.playerInfo.videoData.video_id == video) {
                        player.playVideo();
                    } else {
                        player.loadVideoById(video);
                    }

                } else {
                    player = new YT.Player('player', {

                        playerVars: {
                            'controls': 0,
                            'showinfo': 0,
                            'rel': 0,
                            'autoplay': 0,
                            'playsinline': 1
                        },
                        videoId: video,
                        events: {
                            'onReady': onPlayerReady,
                        }
                    });
                }
            });

            $('#video-modal').on('hidden.bs.modal', function() {
                player.pauseVideo();
            });

            function onPlayerReady() {
                player.playVideo();
            }
        });

        if (jQuery('.simple-dots').length) {
            jQuery('.simple-dots').countdown({
                until: new Date('2021', '0', '17', '23', '59'),
                format: 'DHMS',
                padZeroes: true,
                timezone: +180,
                onExpiry: hideBlock
            });

            var arr = $('.simple-dots').countdown('getTimes'),
                count = 0;

            for (let i = 0; i < arr.length; i++) {
                count += parseInt(arr[i]);
            }
            if (count < 1) {
                hideBlock();
            }

            function hideBlock() {

            }
        }


        //таймер по времени
        var timeSpend = 1000 * 60 * 60 * 24;

        if (localStorage.getItem('countdown') != undefined) {
            var interval = ((new Date()).getTime() - localStorage.getItem('countdown'));

            if (interval > timeSpend) {
                timeSpend = 0;
            } else {
                timeSpend -= interval;
            }
        } else {
            localStorage.setItem('countdown', (new Date()).getTime());
        }

        jQuery('.clock').countdown({
            until: timeSpend / 1000,
            format: 'HMS',
            compact: true,
            padZeroes: true,
            onExpiry: butthide,
        });

        function butthide() {

        }

        $(document).ready(function() {
            if (timeSpend < 1) {
                butthide()
            }
        });
    </script>

</body>

</html>