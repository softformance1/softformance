<footer class="footer bg-dark py-3 fixed-bottom">
	<div class="container text-light">
		<div class="row align-items-center small">
			<div class="col">CL9 System. <?php echo date('Y'); ?> © Copyright</div>
			<div class="col-auto ms-auto">
				<a href="https://lpe.colin990.com/" target="_blank" class="text-light">lpe.colin990.com</a>
			</div>
		</div>
	</div>
</footer>