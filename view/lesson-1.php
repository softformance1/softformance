<!DOCTYPE html>
<html>

<head>
	<?php include 'blocks/head.php'; ?>
</head>

<body class="lesson">
	<div class="wrapper full">

		<section class="lesson-inner text-center">
			<div class="container">
				<div class="logo text-center">
					<img src="<?php echo $baseurl; ?>/assets/images/logo.svg" alt="logo">
				</div>
				<div class="for-title">
					<p class="text-uppercase color-orange fs-xl-24 fs-md-20 fs-16">4-day online course by Vitaliy Podoba:</p>
					<h1 class="title fw-bold fs-xl-56 fs-md-40 fs-25">
						How to create your own application: <br class="d-none d-md-inline"> from idea to first clients
					</h1>
				</div>
				<div class="for-video">
					<div class="text fw-bold">
						<p><span class="text-decoration-underline">Lesson 1</span></p>
						<p>What problems do software customers face, and how to avoid them</p>
					</div>
					<div class="main-video video-block mt-4">
						<div class="ratio ratio-16x9">
							<div id="player"></div>
							<div class="to-play" style="background-image: url(<?php echo $baseurl; ?>/assets/images/lesson-1.jpg)">
								<img src="<?php echo $baseurl; ?>/assets/images/play-1.svg" alt="play">
							</div>
						</div>
					</div>
					<div class="min-videos">
						<div class="row">
							<div class="col-12 col-sm-6 col-lg-3">
								<div class="min-video video-block active" data-id="X3FSxCvHbl0" style="background-image: url(<?php echo $baseurl; ?>/assets/images/lesson-1.jpg)">
									<div class="icon">
										<img src="<?php echo $baseurl; ?>/assets/images/play-1.svg" alt="play">
									</div>
									<p>Lesson 1</p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-lg-3">
								<div class="min-video video-block locked" style="background-image: url(<?php echo $baseurl; ?>/assets/images/lesson-2.jpg)">
									<div class="icon">
										<img src="<?php echo $baseurl; ?>/assets/images/locked.svg" alt="locked">
									</div>
									<p>Lesson 2</p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-lg-3">
								<div class="min-video video-block locked" style="background-image: url(<?php echo $baseurl; ?>/assets/images/lesson-3.jpg)">
									<div class="icon">
										<img src="<?php echo $baseurl; ?>/assets/images/locked.svg" alt="locked">
									</div>
									<p>Lesson 3</p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-lg-3">
								<div class="min-video video-block locked" style="background-image: url(<?php echo $baseurl; ?>/assets/images/lesson-4.jpg)">
									<div class="icon">
										<img src="<?php echo $baseurl; ?>/assets/images/locked.svg" alt="locked">
									</div>
									<p>Lesson 4</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="text fs-xl-24 fs-md-20 fs-14 mt-4 mt-md-5">
					<p>Access to video No. 2 will be sent to you <span class="fw-bold color-orange">by email or messenger <br class="d-none d-md-inline"> tomorrow</span> so that you can do your homework</p>
				</div>
			</div>
		</section>
	</div>

	<?php include 'blocks/scripts.php'; ?>

</body>

</html>