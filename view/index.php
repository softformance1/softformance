<!DOCTYPE html>
<html>

<head>
    <?php include 'blocks/head.php'; ?>
</head>

<body>
    <div class="wrapper">
        <header class="header position-relative">
            <div class="color-dots-wrapper">
                <div class="color-dots cds-1 cd-light"></div>
                <div class="color-dots cds-2 cd-light"></div>
            </div>
            <div class="dots d-light opacity-25"></div>

            <div class="container">
                <div class="h-top">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-auto">
                            <div class="logo text-center">
                                <img src="<?php echo $baseurl; ?>/assets/images/logo.svg" alt="logo">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="social">
                                <a target="_blank" href="#">
                                    <img src="<?php echo $baseurl; ?>/assets/images/sc1.svg" alt="">
                                </a>
                                <a target="_blank" href="#">
                                    <img src="<?php echo $baseurl; ?>/assets/images/sc2.svg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="h-middle">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-7">
                            <div class="hm-left">
                                <div class="for-title">
                                    <div class="tit text-uppercase color-orange fs-xl-28 fs-md-20 fs-14 mb-md-3">
                                        Product Strategy
                                    </div>
                                    <h1 class="title fs-xxl-84 fs-xl-74 fs-md-47 fs-34 fw-bold">
                                        Software Startup <br> Workshop (SSW)
                                    </h1>
                                    <!-- mob. pic -->
                                    <div class="pic d-md-none">
                                        <img src="<?php echo $baseurl; ?>/assets/images/main-pic.png" alt="pic">
                                    </div>
                                    <p class="txt fs-xl-22 fs-md-17 fs-14">
                                        The fastest and most efficient way <br class="d-md-none"> to implement <br class="d-none d-md-inline"> your software idea for <br class="d-md-none"> development
                                    </p>
                                </div>
                                <div class="order mt-3 mt-md-4">
                                    <a class="cl-btn" href="#">
                                        Order WorkShop now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- desk. pic -->
                        <div class="col-12 col-md-5 d-none d-md-block">
                            <div class="hm-right">
                                <div class="pic">
                                    <img src="<?php echo $baseurl; ?>/assets/images/main-pic.png" alt="pic">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="h-bottom">
                    <div class="row">
                        <div class="col-12 col-xl-auto align-self-center mb-3 mb-xl-0">
                            <div class="tit fs-xl-24 fs-md-20 fs-16 fw-bold ff-titillium">
                                With a Workshop <br class="d-none d-xl-inline"> you will get:
                            </div>
                        </div>
                        <div class="col">
                            <div class="hb-row fs-xl-16 fs-14">
                                <div class="row flex-nowrap flex-lg-wrap">
                                    <div class="col-auto col-lg">
                                        <div class="hb-block">
                                            <div class="cube"></div>
                                            <p>Cost reduce & speed boost (&lt;3 months to launch)</p>
                                        </div>
                                    </div>
                                    <div class="col-auto col-lg">
                                        <div class="hb-block">
                                            <div class="cube"></div>
                                            <p>Generated and Validated Proper Software Idea</p>
                                        </div>
                                    </div>
                                    <div class="col-auto col-lg">
                                        <div class="hb-block">
                                            <div class="cube"></div>
                                            <p>Product strategy definition- a clear plan for each part of software development</p>
                                        </div>
                                    </div>
                                    <div class="col-auto col-lg">
                                        <div class="hb-block">
                                            <div class="cube"></div>
                                            <p>Risk Reduction: Establishing business process and preparing the technical part</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main>
            <div class="white-block position-relative overflow-hidden">
                <div class="color-dots-wrapper">
                    <div class="color-dots cds-1"></div>
                    <div class="color-dots cds-2"></div>
                </div>
                <div class="dots d-blue"></div>
                <section class="profit">
                    <div class="container">
                        <div class="for-title text-md-center">
                            <h2 class="title fs-xl-60 fs-md-45 fs-34 fw-bold lh-1">
                                <span class="color-orange">Receiving a profit –</span> <br class="d-none d-md-inline"> the main goal of any business
                            </h2>
                            <div class="text mt-4 mt-md-5 fs-xl-22 fs-md-18 fs-16">
                                <p>And on what business strategy you define, the amount of profit depends. <br class="d-none d-md-inline"> <span class="fw-semi">The business itself depends on the correct strategy!</span> Therefore, it is so important <br class="d-none d-md-inline"> for a business to have the most effective strategy.</p>
                                <p class="mt-3">Strategy, which will ultimately form the basis of software development - <br class="d-none d-md-inline"> another tool that increases sales, and therefore profits</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="for">
                    <div class="container">
                        <div class="f-row">
                            <div class="row align-items-center">
                                <div class="col-12 col-lg-6">
                                    <div class="f-left">
                                        <h2 class="title fs-xl-70 fs-md-50 fs-35 fw-bold ff-titillium lh-1">
                                            This offer is <br> for you, if you:
                                        </h2>
                                        <div class="f-blocks fs-xl-24 fs-md-20 fs-18 mt-4 mt-md-5">
                                            <div class="f-block d-flex align-items-center mb-3">
                                                <div class="icon flex-shrink-0 me-3">
                                                    <img src="<?php echo $baseurl; ?>/assets/images/f1.svg" alt="icon">
                                                </div>
                                                <p>Business owners</p>
                                            </div>
                                            <div class="f-block d-flex align-items-center mb-3">
                                                <div class="icon flex-shrink-0 me-3">
                                                    <img src="<?php echo $baseurl; ?>/assets/images/f2.svg" alt="icon">
                                                </div>
                                                <p>Experts</p>
                                            </div>
                                            <div class="f-block d-flex align-items-center">
                                                <div class="icon flex-shrink-0 me-3">
                                                    <img src="<?php echo $baseurl; ?>/assets/images/f3.svg" alt="icon">
                                                </div>
                                                <p>Training consultants</p>
                                            </div>
                                        </div>
                                        <!-- desk. btn -->
                                        <div class="order mt-5 d-none d-md-block">
                                            <a class="cl-btn" href="#">
                                                Yes it’s me! I want to order a workshop
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 mt-3 mt-lg-0">
                                    <div class="f-right">
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="fr-block">
                                                    <div class="pic">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/fr1.svg" alt="pic">
                                                    </div>
                                                    <div class="icon mb-3">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/ok.svg" alt="ok">
                                                    </div>
                                                    <p><span class="fw-semi">Want to know</span> what it takes to create and successfully run your own software</p>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="fr-block">
                                                    <div class="pic">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/fr2.svg" alt="pic">
                                                    </div>
                                                    <div class="icon mb-3">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/ok.svg" alt="ok">
                                                    </div>
                                                    <p><span class="fw-semi">Want to automate your</span> business: to make them as convenient to scale and serve a large amount of clients</p>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="fr-block">
                                                    <div class="pic">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/fr3.svg" alt="pic">
                                                    </div>
                                                    <div class="icon mb-3">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/ok.svg" alt="ok">
                                                    </div>
                                                    <p><span class="fw-semi">Get a detailed step by step plan</span>, with requirements, and estimates to launch your first software version</p>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="fr-block">
                                                    <div class="pic">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/fr4.svg" alt="pic">
                                                    </div>
                                                    <div class="icon mb-3">
                                                        <img src="<?php echo $baseurl; ?>/assets/images/ok.svg" alt="ok">
                                                    </div>
                                                    <p><span class="fw-semi">Get help</span> validating your software idea</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- mob. btn -->
                        <div class="order mt-5 d-md-none">
                            <a class="cl-btn" href="#">
                                Yes it’s me! I want to order a workshop
                            </a>
                        </div>
                    </div>
                </section>

                <section class="help">
                    <div class="container">
                        <div class="row align-items-center position-relative">
                            <div class="col-12 col-md-6 d-flex justify-content-center">
                                <div class="pic">
                                    <img src="<?php echo $baseurl; ?>/assets/images/help-pic.png" alt="pic">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="h-right">
                                    <div class="for-title">
                                        <h2 class="title fs-xl-60 fs-md-40 fs-34 fw-bold ff-titillium lh-1">
                                            How <br class="d-md-none"> <span class="color-orange">we can <br class="d-md-none"> help you</span> increase <br class="d-md-none"> your bottom line
                                        </h2>
                                        <p class="fs-xl-18 fs-16 mt-4 mt-md-5"><span class="fw-semi">Software Startup Workshop</span> — this is not an educational program, this is a done-for-you service. The result is a perfectly planned software idea, with all terms, prices, numbers, steps and technologies</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="modules">
                    <div class="container">
                        <div class="slider">
                            <div class="swiper-pagination modules-pagination ff-titillium fs-xl-12 fs-10 text-uppercase">
                                <div class="pag-1">Module 1</div>
                                <div class="pag-2">Module 2</div>
                                <div class="pag-3">Module 3</div>
                                <div class="pag-4">Module 4</div>
                                <div class="pag-5">Module 5</div>
                                <div class="pag-6">Bonus Module</div>
                            </div>
                            <div class="swiper-container swiper-modules">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Module 1
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Strategic business modeling and positioning
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will review the templates.</p>
                                                        <p>Will create Unique Selling Proposition.</p>
                                                        <p>Will define the business model, key indicators, sources of income.</p>
                                                        <p>Will find a segment of clients and channels of potential clients.</p>
                                                        <p>Will consider an undeserved (maybe unnoticed) advantage.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">
                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result: </div>
                                                            <div class="check-list">
                                                                <p>A clear understanding of your business model and the critical factors that contribute to its success.</p>
                                                                <p>Later on this will elp determine the strategy of your product.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Module 2
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Defining Product Strategy
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will use the “lean startup” framework.</p>
                                                        <p>Will analyze the habitual addiction and the engines of social catalysts.</p>
                                                        <p>Will develop a potential customer acquisition funnel with the help of strategies of attraction, activation, retention, income and referral system.</p>
                                                        <p>Will create “Hooked Canvas” and devine viral media channels.</p>
                                                        <p>Will define the first target customers.</p>
                                                        <p>Will figure out how to build your own Community.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">
                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result:</div>
                                                            <div class="check-list">
                                                                <p>A clear understanding of your business model and the critical factors that contribute to its success. In the future, this will help determine the strategy of your product.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Module 3
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Requirements for attraction
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will create specifications in the format of «User Stories (features)» and eligibility criteria.</p>
                                                        <p>Will decide on the platform.</p>
                                                        <p>Evaluate the requirements for the server and API.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">

                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result:</div>
                                                            <div class="check-list">
                                                                <p>Get a clear roadmap for the development phase. At this stage, you prioritize based on :</p>
                                                                <ul class="mt-2">
                                                                    <li>your budget</li>
                                                                    <li>what brings the greatest return on investment.</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Module 4
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Technical overview
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will highlight potential technical issues and risk areas.</p>
                                                        <p>Will evaluate alternative options for functions (if needed) and receive a detailed analysis of the project from the technical team.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">
                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result:</div>
                                                            <div class="check-list">
                                                                <p>Well-calibrated forecast for making informed business decisions with technical confidence. This will help you plan your path to implementation.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Module 5
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Making an MSP
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will evaluate User Stories (features) against your key business goals.</p>
                                                        <p>Will get rid of the clutter and prioritize the most valuable features to determine the true MSP.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">
                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result:</div>
                                                            <div class="check-list">
                                                                <p>You will receive a condensed version of the product that reflects your realistic budget and key business goals.</p>
                                                                <p>You can immediately launch this version to the market to confirm your hypotheses.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="module">
                                            <div class="m-top">
                                                <div class="tit fs-xl-18 fs-md-16 fs-14 text-uppercase color-orange mb-2">
                                                    Bonus Module
                                                </div>
                                                <div class="title fs-xl-42 fs-md-35 fs-30 fw-bold color-blue ff-titillium lh-1">
                                                    Post-workshop and technical analysis and finalised User Stories and final expenses
                                                </div>
                                                <div class="for-list mt-4">
                                                    <div class="l-tit fw-semi fs-xl-18 fs-16 mb-3">
                                                        Together we:
                                                    </div>
                                                    <div class="squad-list">
                                                        <p>Will assemble the appropriate technical team to understand the whole concept and develop the best way to create your product. Our company will provide structural support for MSP and future functions.</p>
                                                        <p>Will present you the final User Stories based on the MSP and prepare a cost breakdown.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-bottom">
                                                <div class="row align-items-end">
                                                    <div class="col-12 col-md">
                                                        <div class="mb-left">
                                                            <div class="tit fw-semi fs-xl-18 fs-16 mb-3">Your result:</div>
                                                            <div class="check-list">
                                                                <p>Through examination of your project to eliminate potential risk areas. We prepare a plan for stepwise software development.</p>
                                                                <p>A clear understanding of the use of the budget.</p>
                                                                <p>Customized User Stories according to your goals and budget.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-auto mt-5 mt-md-0">
                                                        <div class="mb-right">
                                                            <div class="order">
                                                                <a class="cl-btn" href="#">
                                                                    BUY Software Startup Workshop NOW
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <section class="who position-relative overflow-hidden">
                <div class="color-dots-wrapper">
                    <div class="color-dots cds-1 cd-light"></div>
                    <div class="color-dots cds-2 cd-light"></div>
                </div>
                <div class="dots d-light opacity-25"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-4">
                            <div class="for-title">
                                <h2 class="title fs-xl-70 fs-md-50 fs-40 fw-bold ff-titillium lh-1">
                                    Who are we?
                                </h2>
                                <p class="mt-3 mt-md-5 fs-xl-22 fs-md-18 fs-16"><span class="fw-semi text-decoration-underline">SoftFormance</span> — are the company that in 11 years has helped more than 200 clients to implement projects, earn money and save budgets</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-md-center">
                            <div class="for-logo d-inline-block">
                                <div class="logo">
                                    <img src="<?php echo $baseurl; ?>/assets/images/logo-big.svg" alt="logo">
                                </div>
                                <div class="symbol">
                                    <img src="<?php echo $baseurl; ?>/assets/images/symbol-big.svg" alt="symbol">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="w-block">
                                <div class="icon mb-3">
                                    <img src="<?php echo $baseurl; ?>/assets/images/who-icon.svg" alt="icon">
                                </div>
                                <div class="for-list">
                                    <div class="tit fw-semi">
                                        For 11 years we have accumulated a lot of experience, which makes it possible to:
                                    </div>
                                    <ul class="mt-3">
                                        <li>implement projects in 2-3 months</li>
                                        <li>fit into the budget</li>
                                        <li>minimize risk</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="white-block position-relative overflow-hidden">
                <div class="color-dots-wrapper">
                    <div class="color-dots cds-1"></div>
                    <div class="color-dots cds-2"></div>
                </div>
                <div class="dots d-blue"></div>
                <section class="founder">
                    <div class="container">
                        <div class="f-inner">
                            <div class="row flex-md-row-reverse">
                                <div class="col-12 col-md-6">
                                    <div class="f-block">
                                        <div class="info ff-titillium">
                                            <div class="tit color-orange text-uppercase fs-xl-20 fs-md-17 fs-12 mb-2">
                                                Founder of SoftFormance
                                            </div>
                                            <div class="title fs-xl-70 fs-md-55 fs-40 fw-bold lh-1">
                                                Vitaly Podoba
                                            </div>
                                        </div>
                                        <div class="squad-list fs-xl-18 fs-16">
                                            <p>CEO and founder of software development company SoftFormance</p>
                                            <p>Former Python/Django programmer</p>
                                            <p>More than 15 years of experience in IT</p>
                                            <p>Author of the book “Web Development with Python and Django for Beginners”</p>
                                            <p>Core Contributor to number of open source projects <br class="d-none d-xl-inline"> (e.g. Plone CMS)</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 align-self-end text-center mt-4 mt-md-0">
                                    <div class="photo">
                                        <img class="mw-100" src="<?php echo $baseurl; ?>/assets/images/founder.png" alt="Vitaly Podoba">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="cost text-center">
                    <div class="container">
                        <div class="for-title">
                            <h2 class="title fs-xl-60 fs-md-45 fs-30 fw-bold ff-titillium lh-1">
                                What is the cost of a workshop <br class="d-none d-md-inline"> required for business development?
                            </h2>
                            <p class="mt-3 mt-md-4 fs-xl-22 fs-md-20 fs-16">We have calculated and determined the cost — <span class="fw-semi">$8 000.</span> <br class="d-none d-md-inline"> But only within ___ hours we are ready to offer it to you for a ___%</p>
                        </div>
                        <div class="prices text-uppercase">
                            <div class="row gx-md-5 justify-content-center">
                                <div class="col-6 col-lg-5 text-md-end">
                                    <div class="price old text-decoration-line-through fs-xl-40 fs-md-30 fs-24 fw-bold mt-2 pt-1 pt-xl-0">$8 000</div>
                                </div>
                                <div class="col-6 col-lg-5 text-md-start">
                                    <div class="price new lh-1 color-orange fs-xl-70 fs-md-60 fs-50 fw-bold">$970</div>
                                    <p class="fs-xl-20 fs-md-18 fs-14 text-uppercase opacity-50">benefit $7 300</p>
                                </div>
                            </div>
                        </div>
                        <div class="buttons-row mt-4 ff-titillium">
                            <div class="row gx-md-5 justify-content-center">
                                <div class="col-12 col-md-6 col-lg-5 text-md-end">
                                    <div class="order d-inline-block">
                                        <a class="cl-btn" href="#">
                                            Get a workshop and apply it to your business
                                        </a>
                                        <p class="fs-md-12 fs-8 text-center mt-2 opacity-50">By clicking on the button, you agree to the Offer Agreement</p>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-5 mt-3 mt-md-0 text-md-start">
                                    <div class="order">
                                        <a class="cl-btn btn-blue" href="#">
                                            Have questions? Write us on WhatsApp
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="for-clock mt-4 d-md-flex justify-content-center justify-content-center">
                            <p class="fs-18 mb-2 mb-md-0 me-md-3">The promotion is valid for:</p>
                            <div class="clock simple-dots fw-semi fs-22"></div>
                        </div>
                    </div>
                </section>
            </div>

            <section class="warranty position-relative overflow-hidden">
                <div class="color-dots-wrapper">
                    <div class="color-dots cds-1 cd-light"></div>
                    <div class="color-dots cds-2 cd-light"></div>
                </div>
                <div class="dots d-light opacity-25"></div>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-6">
                            <div class="w-left">
                                <div class="for-title">
                                    <h2 class="title fs-xl-70 fs-md-50 fs-38 ff-titillium fw-bold">
                                        100% Guarantee
                                    </h2>
                                    <p class="fs-xl-22 fs-md-18 fs-16 mt-3 mt-md-4">If after the first steps of collaboration you decide that this proposed strategy is not for you, then <span class="fw-semi">we will return your investment</span> without any questions</p>
                                </div>
                                <!-- mob. pic -->
                                <div class="for-phone d-md-none">
                                    <div class="phone">
                                        <img src="<?php echo $baseurl; ?>/assets/images/phone.png" alt="phone">
                                    </div>
                                    <div class="symbol">
                                        <img src="<?php echo $baseurl; ?>/assets/images/sybmol-huge.png" alt="sybmol">
                                    </div>
                                </div>
                                <div class="w-block p-3 p-md-4">
                                    <div class="wb-top d-flex">
                                        <div class="icon flex-shrink-0 me-2 me-md-3">
                                            <img src="<?php echo $baseurl; ?>/assets/images/comment.svg" alt="comment">
                                        </div>
                                        <p class="fs-xl-18 fs-16">Have a look at the reviews on <br class="d-none d-xl-inline"> the strategies we have developed in profile @_________</p>
                                    </div>
                                    <div class="order mt-4">
                                        <a class="cl-btn w-100 fs-xl-22 fs-md-20 fs-18" href="#">
                                            Have a look
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-none d-md-block text-md-center">
                            <!-- desk. pic -->
                            <div class="for-phone position-relative">
                                <div class="phone">
                                    <img src="<?php echo $baseurl; ?>/assets/images/phone.png" alt="phone">
                                </div>
                                <div class="symbol">
                                    <img src="<?php echo $baseurl; ?>/assets/images/sybmol-huge.png" alt="sybmol">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="links text-lg-center fs-12">
                        <div class="row align-items-center">
                            <div class="col-12 col-md mb-3 mb-md-0">
                                <p>© All rights reserved, SoftFormance, <?php echo date('Y'); ?></p>
                            </div>
                            <div class="col-6 col-md text-lg-center">
                                <div class="link">
                                    <a target="_blank" href="">Privacy Policy</a>
                                </div>
                            </div>
                            <div class="col-6 col-md text-lg-center">
                                <div class="link">
                                    <a target="_blank" href="">Offer Agreement</a>
                                </div>
                            </div>
                            <div class="col-12 col-lg-auto for-social text-lg-end mb-3 mb-lg-0">
                                <div class="social">
                                    <a target="_blank" href="#">
                                        <img src="<?php echo $baseurl; ?>/assets/images/sc1.svg" alt="">
                                    </a>
                                    <a target="_blank" href="#">
                                        <img src="<?php echo $baseurl; ?>/assets/images/sc2.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </main>

    </div>

    <?php include 'blocks/modals.php'; ?>

    <?php include 'blocks/scripts.php'; ?>

    <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="ratio ratio-16x9">
                    <div id="player"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;



        $(document).ready(function() {
            $('#video-modal').on('shown.bs.modal', function(e) {
                let button = $(e.relatedTarget),
                    video = button.data('video');

                if (player) {
                    if (player.playerInfo.videoData.video_id == video) {
                        player.playVideo();
                    } else {
                        player.loadVideoById(video);
                    }

                } else {
                    player = new YT.Player('player', {

                        playerVars: {
                            'controls': 0,
                            'showinfo': 0,
                            'rel': 0,
                            'autoplay': 0,
                            'playsinline': 1
                        },
                        videoId: video,
                        events: {
                            'onReady': onPlayerReady,
                        }
                    });
                }
            });

            $('#video-modal').on('hidden.bs.modal', function() {
                player.pauseVideo();
            });

            function onPlayerReady() {
                player.playVideo();
            }
        });

        if (jQuery('.simple-dots').length) {
            jQuery('.simple-dots').countdown({
                until: new Date('2021', '0', '17', '23', '59'),
                format: 'DHMS',
                padZeroes: true,
                timezone: +180,
                onExpiry: hideBlock
            });

            var arr = $('.simple-dots').countdown('getTimes'),
                count = 0;

            for (let i = 0; i < arr.length; i++) {
                count += parseInt(arr[i]);
            }
            if (count < 1) {
                hideBlock();
            }

            function hideBlock() {

            }
        }


        //таймер по времени
        var timeSpend = 1000 * 60 * 60 * 24;

        if (localStorage.getItem('countdown') != undefined) {
            var interval = ((new Date()).getTime() - localStorage.getItem('countdown'));

            if (interval > timeSpend) {
                timeSpend = 0;
            } else {
                timeSpend -= interval;
            }
        } else {
            localStorage.setItem('countdown', (new Date()).getTime());
        }

        jQuery('.clock').countdown({
            until: timeSpend / 1000,
            format: 'HMS',
            compact: true,
            padZeroes: true,
            onExpiry: butthide,
        });

        function butthide() {

        }

        $(document).ready(function() {
            if (timeSpend < 1) {
                butthide()
            }
        });
    </script>

</body>

</html>