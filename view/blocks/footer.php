<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md col-12 mb-3 mb-md-0 text-center text-md-left">
				CL9 System. <?php echo date('Y'); ?> © Все права защищены	
			</div>
			<div class="col-md-auto col-12 ms-md-auto ms-0 text-center">
				<a href="#" target="_blank">Политика конфиденциальности</a>
			</div>
		</div>
	</div>
</footer>