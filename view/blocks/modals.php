<div class="modal fade" id="reg-email" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="forma">
                <div class="tit fw-bold fs-xl-30 fs-md-25 fs-18 text-uppercase text-center mb-4">
                    Enter your data
                </div>
                <form action="https://www.mail.mariyasolodar.com/add_subscriber.html" method="post">
                    <div class="inputblock mb-3">
                        <input class="form-control" placeholder="Your name" name="name" type="text" required>
                    </div>
                    <div class="inputblock mb-3">
                        <input class="form-control" placeholder="Your phone" name="custom_mob" type="tel" pattern="[0-9,+]{10,}" required>
                    </div>
                    <div class="inputblock mb-3">
                        <input class="form-control" placeholder="Your e-mail" name="email" type="email" required>
                    </div>
                    <input type="hidden" name="campaign_token" value="5L" />
                    <input type="hidden" name="start_day" value="0" />
                    <input type="hidden" name="thankyou_url" value="<?php echo $baseurl; ?>/preorder-thanks" />
                    <input type="hidden" name="utm_campaign" value="<?php echo $_GET['utm_campaign']; ?>">
                    <input type="hidden" name="utm_content" value="<?php echo $_GET['utm_content']; ?>">
                    <input type="hidden" name="utm_medium" value="<?php echo $_GET['utm_medium']; ?>">
                    <input type="hidden" name="utm_source" value="<?php echo $_GET['utm_source']; ?>">
                    <input type="hidden" name="utm_term" value="<?php echo $_GET['utm_term']; ?>">
                    <input type="hidden" name="utm_group" value="<?php echo $_GET['utm_group']; ?>">
                    <input name="custom_http_referer2" type="hidden" value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
                    <div class="order">
                        <button class="send cl-btn w-100" type="button">
                            Send
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reg-messangers" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="tit fw-bold fs-xl-30 fs-md-25 fs-18 text-uppercase text-center mb-4">
                Choose messenger
            </div>
            <div class="socials text-uppercase d-flex justify-content-center">
                <div class="link">
                    <a class="tg" href="#">
                        <div class="icon flex-shrink-0">
                            <img src="<?php echo $baseurl; ?>/assets/images/scc2.svg" alt="icon">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>