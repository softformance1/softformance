<!DOCTYPE html>
<html>
	<head>
		<?php include 'blocks/head.php'; ?>
	</head>
	<body>
		<div class="wrapper">			
			<header class="header">
				<div class="container">
					<div class="logo mb-3">
						<a href="<?php echo $baseurl; ?>">CL9 System</a>
					</div>
					<div class="title">Thanks</div>
					<p><?php echo $data['item1']['date']; ?></p>
					<p><?php echo htmlspecialchars_decode($data['item1']['slogan']); ?></p>
				</div>
			</header>
			
			<section class="sec-1">
				<div class="container">
					<div class="title">Заголовок</div>
				</div>
			</section>
			
			<section class="sec-2">
				<div class="container">
					<div class="title">Заголовок</div>
				</div>
			</section>
			
			<section class="sec-3">
				<div class="container">
					<div class="title">Заголовок</div>
				</div>
			</section>
			
			<section class="sec-4">
				<div class="container">
					<div class="title">Заголовок</div>
				</div>
			</section>
			
			<section class="sec-5">
				<div class="container">
					<div class="title">Заголовок</div>
				</div>
			</section>

			<?php include 'blocks/footer.php'; ?>
		</div>
		
		<?php include 'blocks/scripts.php'; ?>
		
	</body>
</html>