<?php
/*
	Файл роутинга
	Документация: https://fatfreeframework.com/3.6/routing-engine
*/

# Обработчик ошибок. Под каждую ошибку можно вывести свой шаблон
$f3->set('ONERROR', 'Site::Error', 0);

/*
	Основная функция роута.
	Мы получаем ссылку (пример: /thanks) и исходя из нее
	ищем нужный файл шаблона в папке /view (для "/thanks"
	получится файл "thanks.php". Если линк с вложеностью 
	(пример: /order/pay), то название файла тоже должно
	повторять вложеность и разделять ее в названи файла
	точкой (для "/order/pay" получится файл "order.pay.php")
*/
$f3->route('GET /*', 'Site::Page', 0);

/*
	Админ панель
*/
$f3->route('GET  /admin', 'Panel::Index', 0);
$f3->route('GET  /login', 'Panel::Login', 0);
$f3->route('GET  /registration', 'Panel::Registration', 0);
$f3->route('GET  /logout', 'Auth::Logout', 0);
$f3->route('GET  /admin/config', 'PanelConfig::List', 0);
$f3->route('POST /admin/config', 'PanelConfig::Save', 0);
$f3->route('POST /admin/clear-cache', 'PanelConfig::ClearCache', 0);
$f3->route('POST /registration', 'Auth::Registration', 0);
$f3->route('POST /login', 'Auth::Login', 0);

$f3->route('GET  /admin/items', 'PanelItems::getList', 0);
$f3->route('GET  /admin/item', 'PanelItems::addItem', 0);
$f3->route('POST /admin/item', 'PanelItems::addItem', 0);
$f3->route('POST /admin/item/@alias', 'PanelItems::addItem', 0);
$f3->route('GET  /admin/item/@alias', 'PanelItems::addItem', 0);
$f3->route('GET  /admin/item-delete/@alias', 'PanelItems::deleteItem', 0);