<?php
/*
	Конфигурационный файл
	Документация: https://fatfreeframework.com/3.6/quick-reference#SystemVariables
*/

# Часовой пояс
$f3->set('TZ', 'Europe/Moscow');

# Templates folder
$f3->set('UI', 'view/');

# Temp folder
$f3->set('TEMP', 'assets/temp/');

# Autoload classes form folders:
$f3->set('AUTOLOAD', '
	app/;
	app/core/;
	app/core/Panel/;
	app/core/Auth/;
	app/core/Magic/;
	app/core/Db/;
	app/core/Db/Jig/;
');

# Каталог для логов
$f3->set('LOGS','app/logs/');

# Кем сделано
$f3->set('PACKAGE','CL9 System on Fat-Free Framework');

# Версия фреймфорка и CMS
$f3Ver = $f3->get('VERSION');
$f3->set('VERSION','F3: '.$f3Ver.'; CL9: 2.0');

# Задаем SCHEME
$sheme = isset($_SERVER['HTTPS']) ? "https" : "http";
$f3->set('SCHEME','https');

# Базовый линк сайта ( без слеша в конце, пример: https://colin990.com/page, page - подкаталог, в котором лежит система )
$f3->set('BASEURL',$f3->get('SCHEME').'://'.$f3->get('HOST').$f3->get('BASE'));

# Текущий линк страницы с ючетом параметров
$f3->set('CURURL',$f3->get('SCHEME')."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");

# Корневой каталог сайта на сервере
$f3->set('BASEPATH', dirname( dirname(__FILE__) ) );

/* Получаем часть конфига с базы */
# Получаем поля с базы
$db = new \DB\Jig('app/data/');
$config = $db->read('config.json');

# Debug (0 - 3)
$f3->set('DEBUG', 0);

# CACHE, 1 - ON, 0 - OFF
if( $config['CACHE'] ) {
	$f3->set('CACHE', $config['CACHE']);
} else {
	$f3->set('CACHE', 0);
}

/*
	Настройки оптимизации.
	В даном разделе можно отключить генерацию CSS кода
	из LESS, а так же включить слияние всего CSS \ JS кода
	в один файл соотвественно. При включении даной опции,
	генерируется кеш файлы для CSS и JS в каталоге "cache/".
	Файлы генерируются только 1 раз. Если кеш файл уже существует,
	то генератор не будет его обновлять, пока вы его не удалите
	из каталога "temp/". Сливаются лишь те CSS и JS файлы, что
	заданы через настройки тут: "assets/includes.php"
*/

# Компилировать SCSS в CSS
if( $config['COMPILE_SCSS'] ) {
	$f3->set('COMPILE_SCSS', $config['COMPILE_SCSS']);
} else {
	$f3->set('COMPILE_SCSS', 0);
}

# META для социалок и продвижения

# Название сайта
if( $config['SITENAME'] ) {
	$f3->set('META.sitename', $config['SITENAME']);
} else {
	$f3->set('META.sitename', '');
}

# Заголовок
if( $config['TITLE'] ) {
	$f3->set('META.title', $config['TITLE']);
} else {
	$f3->set('META.title', '');
}

# Описание
if( $config['DESCRIPTION'] ) {
	$f3->set('META.desc', $config['DESCRIPTION']);
} else {
	$f3->set('META.desc', '');
}

# Ключевые слова
if( $config['KEYWORDS'] ) {
	$f3->set('META.keys', $config['KEYWORDS']);
} else {
	$f3->set('META.keys', '');
}

# Картинка для соц. сетей, размер: 1200x630px
$f3->set('META.image', $f3->get('BASEURL').'/assets/images/social.jpg');

// разрешаем загрузку в iframe
$f3->set('XFRAME','GOFORIT');