<?php
/*
	Класс с полезными вспомогательными функциями
*/
class PanelHelper {
	function __construct() {
		
	}
	
	# Удалить c директории все файлы
	static function DeleteFolderFiles( $dir ) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			
			foreach($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir . "/" . $object)) :
						self::DeleteFolderFiles($dir . "/" . $object);
					else :
						unlink($dir . "/" . $object);
					endif;
				}
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	# Проверить, есть ли в директории файлы
	static function IsFolderHasFiles( $dir ) {
		$hasFiles = false;
		
		if (is_dir($dir)) {
			$objects = scandir($dir);
			
			foreach($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir . "/" . $object)) :
						$hasFiles = self::IsFolderHasFiles($dir . "/" . $object);
					else :
						$hasFiles = true;
					endif;
				}
			}
		}
		
		return $hasFiles;
	}
	
	# Получить список типов контента
	static function getContentTypes() {
		if ( !file_exists('app/data/cck.php') ) return false;
		
		$contentTypes = require_once('app/data/cck.php');

		return $contentTypes;
	}
	
	# Получить тип контента по названию
	static function getContentType( $name ) {
		if ( !$name ) return false;
		
		$contentTypes = self::getContentTypes();
		
		if ( !count($contentTypes) ) return false;
		
		$contentType = false;
		foreach( $contentTypes as $ct ) :
			# Если нашли нужный тип контента
			if ( $ct['name'] == $name ) :
				$contentType = $ct;
				break;
			endif;
		endforeach;
		
		return $contentType;
	}
}