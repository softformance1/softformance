<?php
class Panel {
	function __construct() {
		
	}
	
	static function renderPage( $tmplPath, $data = false ){
		$f3 = Base::instance();
		
		# Задаем переменные, которые хотим передать в шаблон
		$f3->set('css', $f3->get('UI_ADMIN_CSS_FILES') );
		$f3->set('js', $f3->get('UI_ADMIN_JS_FILES') );
		$f3->set('meta', $f3->get('META') );
		$f3->set('baseurl', $f3->get('BASEURL') );
		$f3->set('error', $f3->get('ADM_ERROR') );
		$f3->set('message', $f3->get('ADM_MESSAGE') );
		
		# Опциональная переменная с любыми данными (например: поля с БД)
		if( $data ) :
			$f3->set( 'data', $data );
		endif;
		
		# Переменные юзера
		if( $f3->get('SESSION.user.login') ) :
			$f3->set('user.login', $f3->get('SESSION.user.login') );
			$f3->set('user.type', $f3->get('SESSION.user.type') );
		endif;
		
		# Если такой файл шаблона есть, то выводим его, иначе - 404
		if ( file_exists('view/'.$tmplPath) ) :
			echo View::instance()->render($tmplPath);
		else :
			$f3->error(404);
		endif;
	}
	
	# Отображение главной страницы панели
	static function Index(){
		$f3 = Base::instance();
		
		# Если пользователь залогинен
		if ( Auth::isUserLoggedIn() ) :

			self::renderPage('admin/panel.php');
			
		# Если пользователь не залогинен
		else :
			
			$f3->reroute('/login');
		
		endif;
	}
	
	static function Login(){
		$f3 = Base::instance();
		
		# Если пользователь залогинен
		if ( Auth::isUserLoggedIn() ) :

			$f3->reroute('/admin');
			
		# Если пользователь не залогинен
		else :
			
			self::renderPage('admin/login.php');
		
		endif;
	}
	
	static function Registration(){
		$f3 = Base::instance();
		
		# Если пользователь залогинен
		if ( Auth::isUserLoggedIn() && Auth::isUserHasRights('admin') ) :
			
			self::renderPage('admin/registration.php');
			
		# Если пользователь не залогинен
		else :
			
			$f3->reroute('/admin');
		
		endif;
	}
}