<?php
class PanelConfig extends Panel {
	
	function __construct() {
		
	}
	
	# Отображение списка полей
	static function List(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$f3->reroute('/login');
		endif;
		
		# Получаем поля с базы
		$db = new \DB\Jig('app/data/');
		
		$config = $db->read('config.json');
		$hasCache = PanelHelper::IsFolderHasFiles('assets/temp');
		
		$data = new stdClass();
		$data->config = $config;
		$data->cache = $hasCache;
		
		parent::renderPage('admin/config.php',$data);
	}
	
	static function Save(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$f3->reroute('/login');
		endif;
		
		# Поля мета даных
		$SITENAME 		= $f3->get('POST.SITENAME');
		$TITLE 			= $f3->get('POST.TITLE');
		$DESCRIPTION 	= $f3->get('POST.DESCRIPTION');
		$KEYWORDS 		= $f3->get('POST.KEYWORDS');
		
		# Поля конфига
		$CACHE 			= $f3->get('POST.CACHE');
		$COMPILE_SCSS 	= $f3->get('POST.COMPILE_SCSS');
		
		# Получаем поля с базы
		$db = new \DB\Jig('app/data/');
		
		$confArr = array(
			'SITENAME' => $SITENAME,
			'TITLE' => $TITLE,
			'DESCRIPTION' => $DESCRIPTION,
			'KEYWORDS' => $KEYWORDS,
			'CACHE' => $CACHE,
			'COMPILE_SCSS' => $COMPILE_SCSS,
		);
		
		$db->write('config.json',$confArr);
		
		$config = $db->read('config.json');
		
		$data = new stdClass();
		$data->config = $config;
		
		$f3->set('ADM_MESSAGE', 'Настройки успешно сохранены' );
		parent::renderPage('admin/config.php',$data);
	}
	
	static function ClearCache(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$answer = array('response' => 'error');
			echo json_encode($answer);
			die();
		endif;
		
		$delFolder = PanelHelper::DeleteFolderFiles('assets/temp');
		
		$answer = array(
			'response' => 'ok',
			'result' => $delFolder
		);
		echo json_encode($answer);
		die();
	}
}