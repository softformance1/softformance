<?php
class PanelItems extends Panel {
	
	function __construct() {
		
	}
	
	# Отображение списка типов контента
	static function getList(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$f3->reroute('/login');
		endif;
		
		# Получаем типы контента
		$contentTypes = PanelHelper::getContentTypes();
		
		# Получаем поля с базы
		$db = new \DB\Jig('app/data/');
		
		$content = $db->read('content.json');
		
		$data = new stdClass();
		$data->items = $content;
		$data->cck = $contentTypes;
		
		if ( $f3->get('GET.deleted') ) :
			$f3->set('ADM_MESSAGE', 'Запись успешно удалена' );
		endif;
		
		parent::renderPage('admin/items.php',$data);
	}
	
	# Получить весь контент для фронта
	static function getItems(){
		$f3 = Base::instance();
		
		# Получаем поля с базы
		$db = new \DB\Jig('app/data/');
		
		$content = $db->read('content.json');
		
		# Меням структуру массива с данными на более удобную для фронта
		if ( count($content) ) :
			$newContent = [];
			
			/*
				Новая структура будет вида:
				$content[алиас типа кон.][название поля] = значение поля.
				Сделано для более удобного доступа к полям на фронте
			*/
			foreach( $content as $item ) :
				$newContent[$item['alias']] = $item['fields'];
			endforeach;
			
			$content = $newContent;
		endif;
		
		return $content;
	}
	
	static function addItem(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$f3->reroute('/login');
		endif;
		
		$data = new stdClass();
		
		$params = $f3->get('PARAMS');
		$alias = $params['alias'];
		
		# Получаем тип контента
		$contentType = ( $f3->get('POST.type') ) ? $f3->get('POST.type') : $f3->get('GET.type');
		$cck = PanelHelper::getContentType($contentType);

		# Если это ПОСТ запрос на сохранение
		if ( $f3->get('POST.type') && $f3->get('POST.alias') && $f3->get('POST.title') ) :
			# Если в ссылке небыло алиаса, то пробуем добавить новую запись
			if ( !$alias ) :
				
				# Получаем объект для работы с БД
				$db = new \DB\Jig('app/data/');
				
				$mapper = new \DB\Jig\Mapper($db, 'content.json');
				
				# Ищем, есть ли в базе запись с таким же алиасом
				$existedItem = $mapper->find(array('@alias = ?',$f3->get('POST.alias')));
				
				# Если запись с таким же алиасом есть, то ошибка, алиас должен быть уникальным
				if ( count($existedItem) ) :
					$f3->reroute('/admin/item?type='.$f3->get('POST.type').'&exist=1');
				endif;
				
				# Получаем кастомные поля записи
				$fields = [];
				foreach( $cck['fields'] as $field ) :
					$fields[$field['name']] = $f3->get('POST.'.$field['name']);
				endforeach;
				
				# Заполняем запись данными и сохраняем
				$mapper->title = $f3->get('POST.title');
				$mapper->alias = $f3->get('POST.alias');
				$mapper->fields = $fields;
				$mapper->type = $contentType;
				$mapper->save();
				$mapper->reset();
				
				# После добавления перенаправляем на страницу записи
				$f3->reroute('/admin/item/'.$f3->get('POST.alias').'?type='.$contentType).'&added=1';
			
			# Елси в ссылке был алиас, пробуем обновить запись
			else :
				
				# Получаем объект для работы с БД
				$db = new \DB\Jig('app/data/');
				
				$mapper = new \DB\Jig\Mapper($db, 'content.json');
				
				# Ищем, есть ли в базе запись с таким же алиасом
				$existedItem = $mapper->find(array('@alias = ?',$f3->get('POST.alias')));
				
				# Если запись с таким же алиасом есть, то обновляем
				if ( count($existedItem) ) :
				
					# Получаем кастомные поля записи
					$fields = [];
					foreach( $cck['fields'] as $field ) :
						$fields[$field['name']] = $f3->get('POST.'.$field['name']);
					endforeach;
				
					# обновляем запись
					$existedItem[0]->title = $f3->get('POST.title');
					$existedItem[0]->alias = $f3->get('POST.alias');
					$existedItem[0]->fields = $fields;
					$existedItem[0]->type = $contentType;
					$existedItem[0]->update();
					$existedItem[0]->reset();
					
					# После добавления перенаправляем на страницу записи
					$f3->reroute('/admin/item/'.$f3->get('POST.alias').'?type='.$contentType.'&updated=1');
					
				# Если такой записи нет, то ошибка
				else :
					$f3->error(404);
				endif;
				
			endif;
			
		# Если это ГЕТ запрос на показ формы
		else :
			# Если есть алиас, то получаем данные по записи
			if ( $alias ) :
				# Получаем объект для работы с БД
				$db = new \DB\Jig('app/data/');
				
				$mapper = new \DB\Jig\Mapper($db, 'content.json');
				
				# Ищем, есть ли в базе запись с таким же алиасом
				$existedItem = $mapper->find(array('@alias = ?',$alias));
				
				# Если запись с таким же алиасом есть, то передаем данные записи
				if ( count($existedItem) ) :
					$data->item['title'] = $existedItem[0]->get('title');
					$data->item['alias'] = $existedItem[0]->get('alias');
					$data->item['fields'] = $existedItem[0]->get('fields');
					$data->item['type'] = $existedItem[0]->get('type');
					
					# Если в ссылке нет типа, но есть запись в БД, то получаем тип с нее
					if ( !$cck ) :
						$cck = PanelHelper::getContentType($data->item['type']);
					endif;
					
					# Добавляем в тип значения доп. полей
					foreach( $cck['fields'] as $i => $cckField ) :
						# Перебираем массив значени и сравниваем с текущим полем в итерации
						foreach( $data->item['fields'] as $itemFieldName => $itemFieldValue ) :
							# Если названия совпадают, значит есть значение этого поля
							if ( $cckField['name'] == $itemFieldName ) :
								# Записываем в тип значение текущего поля
								$cck['fields'][$i]['value'] = $itemFieldValue;
							endif;
						endforeach;
					endforeach;
				endif;
			endif;
			
			$data->cck = $cck;
		
		endif;
		
		if ( $f3->get('GET.exist') ) :
			$f3->set('ADM_ERROR', 'Запись с таким алиасом уже существует' );
		endif;
		
		if ( $f3->get('GET.updated') ) :
			$f3->set('ADM_MESSAGE', 'Запись успешно обновлена' );
		endif;
		
		if ( $f3->get('GET.added') ) :
			$f3->set('ADM_MESSAGE', 'Запись успешно добавлена' );
		endif;
		
		parent::renderPage('admin/item.php',$data);
	}
	
	static function deleteItem(){
		$f3 = Base::instance();
		
		# Если пользователь не залогинен
		if ( !Auth::isUserLoggedIn() ) :
			$f3->reroute('/login');
		endif;
		
		$params = $f3->get('PARAMS');
		$alias = $params['alias'];
		
		if ( !$alias ) return false;
		
		# Получаем объект для работы с БД
		$db = new \DB\Jig('app/data/');
		
		$mapper = new \DB\Jig\Mapper($db, 'content.json');
		
		# Ищем, есть ли в базе запись с таким же алиасом
		$existedItem = $mapper->find(array('@alias = ?',$alias));
		
		# Если запись с таким же алиасом есть, то удаляем
		if ( count($existedItem) ) :
		
			# Удаляем запись
			$existedItem[0]->erase();
			$existedItem[0]->reset();
			
			# После добавления перенаправляем на страницу записи
			$f3->reroute('/admin/items?deleted=1');
			
		# Если такой записи нет, то ошибка
		else :
			$f3->reroute('/admin/items');
		endif;
		
		return;
	}
}