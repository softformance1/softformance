<?php
class PanelExport {
	function __construct() {
		
	}
	static function Generate(){
		$f3 = Base::instance();
		
		$files = self::getFilesFromFolder('view');
		
		if ( count($files) ) {
		
			foreach($files as $file) {
				echo $file.'<br>';
				
				$fileName = basename($file, ".php");
				$html = Site::renderPage($file, false, 1);
				
				if ( $file == 'index.php' ) {
					$dir = '.';
				} else {
					$dir = './'.$fileName;
					
					if ( !is_dir($dir) ) {
						mkdir($dir);
					}
				}
				
				$dir .= '/index.html';
				
				echo $dir.'<br>';
				
				file_put_contents($dir, $html);
			}
		}
		
		die();
	}
	
	# Получить список файлов с директории
	static function getFilesFromFolder( $dir = '', $ext = ['php'] ) {
		$filesList = false;
		
		if (is_dir($dir)) {
			// init result
			$result = array();

			// directory to scan
			$directory = new DirectoryIterator($dir);

			// iterate
			foreach ($directory as $fileinfo) {
				// must be a file
				if ($fileinfo->isFile()) {
					// file extension
					$extension = strtolower(pathinfo($fileinfo->getFilename(), PATHINFO_EXTENSION));
					// check if extension match
					if (in_array($extension, $ext)) {
						$result[] = $fileinfo->getFilename();
					}
				}
			}
			$filesList = $result;
		}
		
		return $filesList;
	}
}