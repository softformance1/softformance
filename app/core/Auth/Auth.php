<?php
include('Password.php');

class Auth {
	function __construct() {
		
	}
	
	static function isUserLoggedIn() {
		$f3 = Base::instance();
		
		# Получаем данные юзера с сессии
		$user = $f3->get('SESSION.user');
		
		# Если в сессии нет данных юзера
		if ( !$user ) :
			
			return false;
			
		endif;
		
		return true;
	}
	
	static function isUserHasRights( $type = '' ) {
		if ( !$type ) return true;
		
		$f3 = Base::instance();
		
		# Получаем данные юзера с сессии
		$user = $f3->get('SESSION.user');
		
		# Если в сессии нет данных юзера
		if ( !$user ) :
			
			return false;
		
		endif;
		
		# Если этот тип пользователя имеет право на просмотр
		if ( $type == $user['type'] ) :
			return true;
		else :
			return false;
		endif;
	}
	
	static function Login() {
		$f3 = Base::instance();
		
		# Получаем с формы логин и пароль
		$login = $f3->get('POST.login');
		$password = $f3->get('POST.password');
		
		# Если логин или пароль пустой - ошибка
		if ( !$login || !$password ) :
			$f3->set('ADM_ERROR', 'Заполните поля формы' );
			
			Panel::renderPage('admin/login.php');
		endif;
		
		$db = new \DB\Jig('app/data/');
		$mapper = new \DB\Jig\Mapper($db, 'users.json');
		
		# Ищем в базе юзера с таким логином
		$user = $mapper->find( array('@login = ?',$login) );
		
		# Если такой логин есть в базе
		if ( $user ) :
			# Получаем данные пользователя с базы
			$userLogin = $user[0]->get('login');
			$userPassword = $user[0]->get('password');
			$userType = $user[0]->get('type');
			
			# Если пароль совпадают, то логиним
			if ( password_verify( $password, $userPassword ) ) :
		
				$f3->set('SESSION.user.login',$userLogin);
				$f3->set('SESSION.user.type',$userType);
		
				$f3->reroute('/admin');
		
			# Если пароль не совпадают - ошибка
			else :
				$f3->set('ADM_ERROR', 'Неверный пароль' );

				Panel::renderPage('admin/login.php');
			endif;
		
		# Если такого логина нет в базе
		else :
			$f3->set('ADM_ERROR', 'Такой пользователь не существует' );
			
			Panel::renderPage('admin/login.php');
		endif;
	}
	
	static function Registration(){
		$f3 = Base::instance();
		
		# Получаем с формы логин и пароль
		$login = $f3->get('POST.login');
		$password = $f3->get('POST.password');
		$password2 = $f3->get('POST.password2');
		
		# Если логин или пароль пустой - ошибка
		if ( !$login || !$password || !$password2 ) :
			$f3->set('ADM_ERROR', 'Заполните все поля формы' );
			
			Panel::renderPage('admin/registration.php');
		endif;
		
		# Если пароли не совпадают - ошибка
		if ( $password != $password2 ) :
			$f3->set('ADM_ERROR', 'Пароли не совпадают' );
			
			Panel::renderPage('admin/registration.php');
		endif;
		
		$db = new \DB\Jig('app/data/');
		$mapper = new \DB\Jig\Mapper($db, 'users.json');
		
		# Ищем юзера с таким логином
		$list = $mapper->find( array('@login = ?',$login) );
		
		# Если такой логин есть, то ошибка
		if ( $list ) :
			$userLogin = $list[0]->get('login');
			$f3->set('ADM_ERROR', 'Такой пользователь уже существует: '.$userLogin );
			
			Panel::renderPage('admin/registration.php');
		
		# Если такого логина нет, то регаем
		else :
			$passwordHash = password_hash($password, PASSWORD_DEFAULT);
		
			$mapper->set('login',$login);
			$mapper->set('password',$passwordHash);
			$mapper->set('type','manager');
			
			$mapper->insert();
		endif;
		
		$f3->set('ADM_MESSAGE', 'Пользователь '.$userLogin.' успешно зарегистрирован' );
		
		Panel::renderPage('admin/registration.php');
	}
	
	static function Logout() {
		$f3 = Base::instance();
		
		# Удаляем сессию пользователя
		$f3->clear('SESSION.user');
		
		$f3->reroute('/login');
	}
}