<?php
class Site {
	function __construct() {
		
	}
	
	# Основная функция для передачи переменных и вывода страницы
	static function renderPage( $tmplPath, $data = false ){
		$f3 = Base::instance();
		
		# Задаем переменные, которые хотим передать в шаблон
		$f3->set('css', $f3->get('TMPL_CSS') );
		$f3->set('js', $f3->get('TMPL_JS') );
		$f3->set('meta', $f3->get('META') );
		$f3->set('baseurl', $f3->get('BASEURL') );
		$f3->set('pageurl', $f3->get('CURURL') );
		$f3->set('images', $f3->get('BASEURL').'/assets/images' );
		
		# Опциональная переменная с любыми данными (например: поля с БД)
		if( $data ) :
			$f3->set( 'data', $data );
		endif;
		
		# Переменные юзера
		if( $f3->get('SESSION.user.login') ) :
			$f3->set('user.login', $f3->get('SESSION.user.login') );
			$f3->set('user.type', $f3->get('SESSION.user.type') );
		endif;
		
		# Если такой файл шаблона есть, то выводим его, иначе - 404
		if ( file_exists('view/'.$tmplPath) ) :
			echo View::instance()->render($tmplPath);
		else :
			$f3->error(404);
		endif;
	}
	
	# Отображение страницы ошибки
	static function Error(){
		$f3 = Base::instance();
		/*
			ERROR.code - displays the error code (404, 500, etc.)
			ERROR.status - header and page title
			ERROR.text - error context
			ERROR.trace - stack trace
		*/
		$code = $f3->get('ERROR.code');
		
		if ( $code == 404 ) {			
			self::renderPage('404.php');
		} else {
			echo '<pre>Code: '.$f3->get('ERROR.code').'<br>Status: '.$f3->get('ERROR.status').'<br>Text: '.$f3->get('ERROR.text').'<br>Trace: '.$f3->get('ERROR.trace').'</pre>';
		}
	}
	
	# Отображение кастомной страницы исходя из адреса
	static function Page(){
		$f3 = Base::instance();
		$params = $f3->get('PARAMS');
		
		# Если это режим выгрузки в HTML
		if ( $params['*'] == 'dev.php' ) {
			
			$page = ( $f3->get('GET.p') ) ? $f3->get('GET.p') : 'index';
			
			$tmplPath = $page.'.php';
			
			if ( $exportHtml ) {
				//PanelExport::Generate();
				//die('Exported');
			}
			
		} else {
			
			$page = ( $params['*'] ) ? $params['*'] : 'index';
			
			# Получаем предполагаемое название файла шаблона
			$tmplPath = str_replace('/','.',$page).'.php';
		
		}
		
		# Если такой файл шаблона есть, то выводим его, иначе - 404
		if ( file_exists('view/'.$tmplPath) ) :
			# Если в БД есть контент, добавляем его в вывод
			$content = PanelItems::getItems();
			
			# Выводим страницу
			self::renderPage($tmplPath, $content);
		else :
			$f3->error(404);
		endif;
	}
}