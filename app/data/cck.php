<?php
class CCK {
	public static $contenttypes = [
		[
			'name' => 'custom_fields',
			'label' => 'Кастомные поля',
			'fields' => [
				[
					'name' => 'date',
					'type' => 'text',
					'label' => 'Дата вебинара',
					'description' => 'Дата вебинара ( в формате: 20 декабря )',
					'default' => ''
				],
				[
					'name' => 'time',
					'type' => 'text',
					'label' => 'Время вебинара',
					'description' => 'Время вебинара ( в формате: 19:00 МСК )',
					'default' => ''
				],
				[
					'name' => 'slogan',
					'type' => 'textarea',
					'label' => 'Призыв в шапке',
					'description' => 'Коротки текст с призывом зарегистрироваться',
					'default' => ''
				],
				[
					'name' => 'show_banner',
					'type' => 'select',
					'label' => 'Показывать банер',
					'description' => 'Показывать банер с мартышками',
					'default' => '0',
					'options' => [
						'0' => 'Скрыть',
						'1' => 'Показать'
					]
				]
			]
		],
		[
			'name' => 'custom_fields2',
			'label' => 'Другие поля',
			'fields' => [
				[
					'name' => 'date',
					'type' => 'text',
					'label' => 'Дата вебинара',
					'description' => 'Дата вебинара ( в формате: 20 декабря )',
					'default' => ''
				],
				[
					'name' => 'show_banner',
					'type' => 'select',
					'label' => 'Показывать банер',
					'description' => 'Показывать банер с мартышками',
					'default' => '0',
					'options' => [
						'0' => 'Скрыть',
						'1' => 'Показать'
					]
				]
			]
		]
	];
	
	public static function Instance() {
		return self::$contenttypes;
	}
}
return CCK::Instance();