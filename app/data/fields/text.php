<div class="row py-1 align-items-center">
	<div class="col-md-8 mb-3 mb-md-0">
		<b><?php echo $field['label']; ?></b>
		<?php if ( $field['description'] ) : ?>
		<br>
		<small>
			<?php echo $field['description']; ?>
		</small>
		<?php endif; ?>
	</div>
	<div class="col-md-4">
		<div class="form-group m-0">
			<input type="text" name="<?php echo $field['name']; ?>" class="form-control" value="<?php echo $field['value']; ?>">
		</div>
	</div>
</div>