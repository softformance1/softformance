<?php
require_once('plugins/scss/scss.inc.php');
use ScssPhp\ScssPhp\Compiler;

/*
	CL9 System
	Основной класс подсистемы
*/
class CL9 {
	/*
		Запуск стандартных функций,
		которые выполняются каждый раз
	*/
	function __construct() {
		
		$f3 = Base::instance();
		
		if ($_GET['compile'] && $_GET['compile'] == 1) {

			# Если включена опция, то компилируем SCSS в CSS
			if ($f3->get('COMPILE_SCSS')) $this->AutoCompileScss();
			
		}
		
		# Получаем CSS для шаблона
		$css = $this->getCSS();
		$f3->set('TMPL_CSS',$css);
		
		# Получаем JS для шаблона
		$js = $this->getJS();
		$f3->set('TMPL_JS',$js);
		
		return;
	}
	
	/*
		Функция компилирует с SCSS код - CSS код
	*/
	function AutoCompileScss() {
		$f3 = Base::instance();
		$cssFiles = require_once('assets/scss/scss.php');
		
		if ( empty($cssFiles) ) return false;
		
		$scss = new Compiler();
		
		foreach( $cssFiles as $cssFile ){
			
			if ( empty($cssFile['scss']) ) continue;
			
			$scssCode = '';
			$outputFile = $f3->get('BASEPATH').'/assets/css/'.$cssFile['css'];
			
			foreach( $cssFile['scss'] as $scssFile ){
				$fileUrl = $f3->get('BASEPATH').'/assets/scss/'.$scssFile;
				$scssCode .= file_get_contents($fileUrl);				
			}
			
			if ( $scssCode ) {
				$css = $scss->compile($scssCode);
				
				if ( $css ) {
					$minCss = $this->minifyCss($css);

					file_put_contents($outputFile, $minCss);
				}
			}
			
		}
		
		return true;
	}
	
	/*
		Функция возвращает массив с линками на CSS файлы.
		Если в конфиге включено объединение файлов в один,
		то скрипт генерирует CSS кеш файл и возвращает линк
		на него (так же в виде массива)
	*/
	function getCSS() {
		$f3 = Base::instance();
		$css = array();
		
		# Если в списке есть ссылки на файлы
		if ( count( $f3->get('UI_CSS_FILES') ) ) :
			# Перебираем список, проверяем есть ли файл и добавляем его в массив
			foreach( $f3->get('UI_CSS_FILES') as $cssFile ) :
				if ( file_exists( $f3->get('BASEPATH').'/'.$cssFile ) ) :
					$css[] = $f3->get('BASEURL').'/'.$cssFile;
				endif;
			endforeach;
		endif;
		
		return $css;
	}
	
	/*
		Функция возвращает массив с линками на JS файлы
		Если в конфиге включено объединение файлов в один,
		то скрипт генерирует JS кеш файл и возвращает линк
		на него (так же в виде массива)
	*/
	function getJS() {
		$f3 = Base::instance();
		$js = array();
		
		# Если в списке есть ссылки на файлы
		if ( count( $f3->get('UI_JS_FILES') ) ) :
			# Перебираем список, проверяем есть ли файл и добавляем его в массив
			foreach( $f3->get('UI_JS_FILES') as $jsFile ) :
				if ( file_exists( $f3->get('BASEPATH').'/'.$jsFile ) ) :
					$js[] = $f3->get('BASEURL').'/'.$jsFile;
				endif;
			endforeach;
		endif;
		
		return $js;
	}

	function minifyCss($input) {
		if(trim($input) === "") return $input;
		return preg_replace(
			array(
				// Remove comment(s)
				'#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
				// Remove unused white-space(s)
				'#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~]|\s(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
				// Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
				'#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
				// Replace `:0 0 0 0` with `:0`
				'#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
				// Replace `background-position:0` with `background-position:0 0`
				'#(background-position):0(?=[;\}])#si',
				// Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
				'#(?<=[\s:,\-])0+\.(\d+)#s',
				// Minify string value
				'#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
				'#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
				// Minify HEX color code
				'#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
				// Replace `(border|outline):none` with `(border|outline):0`
				'#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
				// Remove empty selector(s)
				'#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
			),
			array(
				'$1',
				'$1$2$3$4$5$6$7',
				'$1',
				':0',
				'$1:0 0',
				'.$1',
				'$1$3',
				'$1$2$4$5',
				'$1$2$3',
				'$1:0',
				'$1$2'
			),
		$input);
	}
}